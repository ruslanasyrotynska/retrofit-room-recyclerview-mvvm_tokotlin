package com.example.zadacha21_withretrofit

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MessageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(messageEntity: List<MessageEntity>)

    @Query("SELECT * FROM MessageEntity")
    fun getMessages(): LiveData<List<MessageEntity>>

    //тут буде запит пошуку у ДБ по ІД
}