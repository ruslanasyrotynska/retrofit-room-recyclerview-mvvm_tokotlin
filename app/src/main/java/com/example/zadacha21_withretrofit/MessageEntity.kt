package com.example.zadacha21_withretrofit

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MessageEntity(
        @PrimaryKey
        var id: Int? = null,
        var userId: Int,
        var title: String,
        var text: String
)