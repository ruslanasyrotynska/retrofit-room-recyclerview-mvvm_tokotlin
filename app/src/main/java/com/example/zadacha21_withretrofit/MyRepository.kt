package com.example.zadacha21_withretrofit

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.zadacha21_withretrofit.MyDatabase.Companion.getInstance
import com.example.zadacha21_withretrofit.RetrofitInstance.retrofit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.*
import java.util.*

class MyRepository(application: Application) {

    //усе з БД
    val allMessages: LiveData<List<MessageEntity>>
    private val messageDao: MessageDao
    private val retrofitInstance: Retrofit

    suspend fun makeApiCallAndInsertDataIntoDB() {

        val parameters: MutableMap<String?, String?> = HashMap()
        parameters["userId"] = "4"
        parameters["_sort"] = "id"
        parameters["_order"] = "desc"

        val jsonPlaceHolderApi = retrofitInstance.create(JsonPlaceHolderApi::class.java)
        //get messages from the server
        val responseBody = jsonPlaceHolderApi.getPosts(parameters).awaitResponse()
        if (responseBody.isSuccessful) {
            //create a list and fill it with messages from server
            val messageApiModels = responseBody.body()!!
            //наповнюю ліст конвертованими даними з сервера
            val listMessageEntity =
                convertedListFromPostApiModelToMessageEntityList(messageApiModels)
            //наповнюємо БД
            messageDao.insert(listMessageEntity)
        }
    }

    private fun convertedListFromPostApiModelToMessageEntityList(postList: List<PostApiModel>): List<MessageEntity> {
        val listMessageEntity: MutableList<MessageEntity> = ArrayList()
        for (postApiModel in postList) {
            val messageEntity = MessageEntity(
                userId = postApiModel.userId,
                id = postApiModel.id,
                title = postApiModel.title,
                text = postApiModel.text
            )
            //наповнюю ліст типу MessageEntity об'єктами, які взяли з ліста типу PostApiModel
            listMessageEntity.add(messageEntity)
        }
        return listMessageEntity
    }

    init {
        val database = getInstance(application)
        messageDao = database.messageDao
        allMessages = messageDao.getMessages()
        retrofitInstance = retrofit!!
    }
}