package com.example.zadacha21_withretrofit

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    //lateinit = я гарантую, що ця зиінна не буде null, але вона буде ініціалізована пізніше
    private lateinit var messageViewModel: MessageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val updateButton = findViewById<Button>(R.id.updateButton)
        updateButton.setOnClickListener(this)

        val adapter = MyRecyclerViewAdapter(this)

        val recyclerView = findViewById<RecyclerView>(R.id.myRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        messageViewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)
        )
            .get(MessageViewModel::class.java)

        /*у методі observe() передаєм два параметри:
      перший - LifecycleOwner - це інтерфейс з методом getLifecycle.Activity і фрагменти в Support Library реалізують цей інтерфейс, тому ми передаєм this.
      LiveData отримає з Activity його Lifecycle і по ньому буде визначати стан Activity.
      Активним вважається стан STARTED або RESUMED. Тобто якщо Activity видно на экрані, то LiveData вважає його активним і буде відправляти дані в його колбек.
      другий параметр - це безпосередньо підписник (колбек), в який LiveData буде відправляти дані.
      В ньому лише один метод onChanged. В нашому випадку туди буде приходити List messageEntities
      */
        messageViewModel.allMessages.observe(this, { messageEntities ->

            //оновлюю адаптер
            adapter.setItems(messageEntities)
        })
    }

    override fun onClick(v: View) {
        messageViewModel.updateMessages() //оскільки актівіті не має мати доступу прямо до репозиторію, то щоб дістатися до його методу makeApiCall()
        // використовуємо посередника - клас MessageViewModel
        Log.d(TAG, "--updated--")
    }

    companion object {
        private const val TAG = "MyActivity"
    }
}