package com.example.zadacha21_withretrofit

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [MessageEntity::class], version = 2)
abstract class MyDatabase : RoomDatabase() {
    abstract val messageDao: MessageDao

    companion object {
        private var instance: MyDatabase? = null
        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): MyDatabase {
            //synchronized means that only one thread at a time can access this method.
            // This way you don't accidentally create two instances of the database when two different threads try to access this method to the same time
            if (instance == null) {
                instance = Room.databaseBuilder(context.applicationContext,
                        MyDatabase::class.java, "note_database")
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return instance!!
        }
    }
}