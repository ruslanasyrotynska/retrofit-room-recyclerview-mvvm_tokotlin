package com.example.zadacha21_withretrofit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MessageViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: MyRepository = MyRepository(application)
    val allMessages: LiveData<List<MessageEntity>> = repository.allMessages

    fun updateMessages() {
        //Any coroutine launched in this scope is automatically canceled if the ViewModel is cleared.
        // Coroutines are useful here for when you have work
        // that needs to be done only if the ViewModel is active
        viewModelScope.launch(IO) {
            repository.makeApiCallAndInsertDataIntoDB()
        }
    }
}