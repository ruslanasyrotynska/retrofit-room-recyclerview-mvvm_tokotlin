package com.example.zadacha21_withretrofit

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.zadacha21_withretrofit.MyRecyclerViewAdapter.MyViewHolder

class MyRecyclerViewAdapter(private val context: Context) : RecyclerView.Adapter<MyViewHolder>() {
    private var messageEntityList: List<MessageEntity>? = null
    fun setItems(messageEntityList: List<MessageEntity>?) {
        this.messageEntityList = messageEntityList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder { //onCreateViewHolder буде створюватися для кожного видимого на екрані елемента
        val view = LayoutInflater.from(context).inflate(R.layout.item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) { //заповнюються даними
        val messageEntity = messageEntityList!![position]
        holder.bind(messageEntity)
    }

    override fun getItemCount(): Int {
        return if (messageEntityList == null) 0 else {
            messageEntityList!!.size
        }
    }

    class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        private val id: TextView = itemView.findViewById(R.id.id)
        private val userID: TextView = itemView.findViewById(R.id.userID)
        private val title: TextView = itemView.findViewById(R.id.title)
        private val text: TextView = itemView.findViewById(R.id.text)

        fun bind(messageEntity: MessageEntity) { //буде наповнювати в'ю даними(item)
            id.text = "ID: " + Integer.toString(messageEntity.id!!)
            userID.text = "UserID: " + Integer.toString(messageEntity.userId)
            title.text = "Title: " + messageEntity.title
            text.text = "Text: " + messageEntity.text
        }
    }
}