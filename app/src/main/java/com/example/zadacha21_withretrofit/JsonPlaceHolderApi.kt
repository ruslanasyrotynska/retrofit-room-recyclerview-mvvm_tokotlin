package com.example.zadacha21_withretrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface JsonPlaceHolderApi {
    @GET("posts")
    fun getPosts(
            @Query("userId") userId: Array<Int?>?,
            @Query("_sort") sort: String?,
            @Query("_order") order: String?
    ): Call<List<PostApiModel?>?>?

    @GET("posts")
    suspend fun getPosts(@QueryMap parameters: Map<String?, String?>?): Call<List<PostApiModel>>
}