package com.example.zadacha21_withretrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    @JvmStatic
    var retrofit: Retrofit? = null
        get() {
            if (field == null) {
                val baseUrl = "https://jsonplaceholder.typicode.com/"
                field = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return field
        }
        private set
}
