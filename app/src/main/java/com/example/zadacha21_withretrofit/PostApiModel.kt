package com.example.zadacha21_withretrofit

import com.google.gson.annotations.SerializedName

data class PostApiModel(//primary constructor
        val userId: Int = 0,
        val id: Int,
        val title: String,
        @SerializedName("body")
        val text: String
)
